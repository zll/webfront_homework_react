import React, {Component} from "react";
import "./theme/style/App.css";
import NewWork from "./NewWork";
import AppItem from "./AppItem";
import TabSwitch from "./modules/common/TabSwitch/TabSwitch";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDialogVisible: false
        }
    }

    addWork() {
        console.log("新建作品");
        this.setState({
            isDialogVisible: true
        });
    }

    _onClickLi(className) {
        alert(className);
    }

    _createLeftClass() {
        let list = ['全部课程', '课程一：静夜思', '课程二：红军长征', '课程三：活字印刷术', '课程四：郑和下西洋', '课程五：梦想之家']
        let result = [];
        list.forEach((className) => {
            result.push((<li><a href="#" onClick={() => this._onClickLi(className)}>{className}</a></li>))
        });
        return result;
    }

    _createCenterItems() {
        let map = new Map([['全部课程', 'nimabi'], ['课程一：静夜思', '静夜思'], ['课程二：红军长征', '红军'],
            ['课程三：活字印刷术', '活字'], ['课程四：郑和下西洋', '活字典'], ['课程五：梦想之家', '梦想']]);
        let result = [];
        for (let [title, value] of map) {
            result.push(<AppItem title={title} name={value} imageClick={() => this._onCenterItemClick(title)}/>)
        }
        return result;
    }

    _onCenterItemClick(title) {
        console.log(title);
    }

    _onNewWorkSubmit(userList) {
        //TODO 提交
        console.log(userList);
        this.setState({
            isDialogVisible: false
        })
    }

    _handleTabClick(id) {
        console.log(id);
    }

    render() {
        var line = {
            height: 1,
            background: '#e2e2e2'
        };
        var dialogAddWork = {
            visibility: this.state.isDialogVisible ? 'visible' : 'hidden',
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%',
            height: '100%'
        };
        const tabs = [
            {id: 1, text: '我的作品'},
            {id: 2, text: '班级作品'}
        ]
        return (

            <div>
                {/*左侧列表*/}
                <div className="page_right">
                    <div className="page_right_head">
                        <div className="user_info">
                            <ul>
                                <li>用户：王小明</li>
                                <li>身份：学生</li>
                            </ul>
                        </div>
                        <div className="select">
                            <select>
                                <option value="android">android</option>
                                <option value="iphone">iphone</option>
                                <option value="blackberry">blackberry</option>
                                <option value="windows">windows</option>
                            </select>
                        </div>
                    </div>

                    <div style={line}></div>

                    <div className="page_right_subtitle">

                        <TabSwitch tabs={tabs} onClick={(id) =>this._handleTabClick(id)}/>
                        <div>
                            <button type="button" id="btn_submit_work">提交作品</button>
                            <button type="button" id="btn_add_work" onClick={this.addWork.bind(this)}>新建作品</button>
                        </div>
                    </div>
                    <div style={line}></div>

                    <div className="page_right_content">
                        <form action="#" method="get">
                            <div className="search_container">
                                <input type="text" name="search" placeholder=" 搜索资源..."/>
                                <button type="button">搜索</button>
                            </div>
                        </form>

                        <div className="page_right_items_container">
                            {this._createCenterItems()}
                        </div>
                    </div>
                    <div className="pagebar">
                        <ul>
                            <li>&lt;</li>
                            <li className="page-select">1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                            <li>5</li>
                            <li>...</li>
                            <li>100</li>
                            <li className="page-next">&gt;</li>
                        </ul>
                    </div>
                </div>
                <div id="dialog_add_work" style={dialogAddWork}>
                    <NewWork onSubmit={(userList) => this._onNewWorkSubmit(userList)}/>
                    {/*<ClassMate/>*/}
                </div>
            </div>
        )
    }
}

export default App;
