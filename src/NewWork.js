import React, {Component} from "react";
import "./theme/style/newwork.css";
import Classmates from "./Classmates";
/**
 * Created by Administrator on 2017/8/23.
 */
class NewWork extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isChildDialogVisible: false, //设置子页是否可见
            userList : []
        }
    }


    _addMembers() {
        let userNames = [];
        for (let item of this.state.userList) {
            userNames.push(
                <div id="tag_user" style={{display: 'flex', alignItems : 'center', height: 20 + 'px', marginLeft: 10 + 'px', marginTop: 5 + 'px'}}>
                    <a style={{color: '#999', fontSize: 12 + 'px'}}>{item}</a>
                    <img src={require("./images/del_author.png")} style={{width: 15 + 'px', height: 15 + 'px', marginLeft: 5 + 'px'}}
                         onClick={() => this._delAuthor(item)}/>
                </div>);
        }
        return userNames;
    }

    _delAuthor(name) {
        let userList = this.state.userList;
        if (userList.indexOf(name) !== -1) {
            this.state.userList.splice(userList.indexOf(name), 1)
        }
        this.setState({userList: this.state.userList});
    }

    _sendMsg(seletedMembers) {
        console.log("=========new Work接收消息=========", seletedMembers);
        this.setState({
            userList: Array.from(seletedMembers),
            isChildDialogVisible : false
        });
    }

    _onBackgroundClick() {
        //postMessage({evtType: "dismiss", sender: "w1"}, window.location.origin);
        console.log("=====new Work发送消息====");
    }

    _onAddUserClick() {
        //document.getElementById('dialog_add_members').style.visibility = 'visible';
        this.setState({
            isChildDialogVisible : true
        });
    }

    render() {
        let style_img_delete = {
            width: 18,
            height: 18,
            position: 'absolute',
            top: 5,
            right: 10
        };

        let style_img_add = {
            width: 20,
            height: 20,
            marginLeft: 15,
            alignSelf: 'flex-end'
        };

        let style_textarea = {
            width: 300,
            borderColor : '#e2e2e2',
            borderStyle : 'solid',
            marginLeft : 10
        };

        let style_btn_commit = {
            fontSize: 11,
            color: '#999',
            marginTop : 5
        };

        let style_dialog_add_member = {
            borderStyle: 'none',
            visibility : this.state.isChildDialogVisible ? 'visible' : 'hidden',
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%',
            height: '100%'
        };

        return (
            <div>
                <div id="work_background" onClick={() => this._onBackgroundClick()}>
                </div>
                <div className="work_dialog">
                    <form action="#" name="work_form" id="work_form">
                        <div className="dialog_head">
                            <li>新建作品</li>
                            <img src={require("./images/close.png")}
                                 style={style_img_delete}/>
                        </div>
                        <div className="dialog_center">

                            <div className="dialog_center_item">
                                <label>资源名称</label>
                                <input type="text" name="resource_name"/>
                            </div>
                            <div className="dialog_center_item">
                                <label>作者</label>
                                <div id="input_type_user">{this._addMembers()}</div>
                                <img id="btn_add_user" src={require("./images/add_author.png")} style={style_img_add} onClick={() => this._onAddUserClick()}/>
                            </div>
                            <div className="dialog_center_item">
                                <label>配图</label>
                                <input type="image" name="image" />
                            </div>
                            <div className="dialog_center_item">
                                <label>简介</label>
                                <textarea
                                    style={style_textarea}
                                    rows="5"
                                    name="desc"/>
                            </div>
                        </div>
                        <div className="dialog_footer">
                            <div id="dialog_footer_btn" onClick={() => this.props.onSubmit(this.state.userList)}>确认提交</div>
                            <div style={style_btn_commit}>提交后显示在班级作品区</div>
                        </div>
                    </form>
                </div>
                {/*<iframe id="dialog_add_members" src="chooseClassmates.html" style={style_dialog_add_member}>*/}
                {/*</iframe>*/}
                <div id="dialog_add_members" style={style_dialog_add_member}>
                    <Classmates click={(members) => this._sendMsg(members)}/>
                </div>
            </div>
        )
    }
}
export default NewWork;