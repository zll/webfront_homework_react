import React, {Component} from "react";
import "./theme/style/classmates.css";
import LeftName from "./ClassmateName";

/**
 * Created by Administrator on 2017/8/23.
 */
class Classmates extends Component {

    constructor(props) {
        super(props);
        this.state = {
            willbeAddName : null,//左侧选中名字
            selectedMembers : new Set(),
            willbeRemovedName : null//右侧选中的名字
        }
    }
    _createLeftName() {
        let nameNodes = [];
        for (let i = 0; i < 15; i++) {
            let name = "名字" + i;
            nameNodes.push((<LeftName name={name} click={(event) => this._onLeftNameClick(event)}/>));
        }
        return nameNodes;
    }

    _onLeftNameClick(event) { // 设置背景色
        event.target.style.background = '#16a4fa';
        event.target.style.color = '#fff';
        console.log(event.target.innerText);
        this.setState({willbeAddName : event.target.innerText});
    }

    _createRightName() {
        let nameNodes = [];
        for (let item of this.state.selectedMembers.values()) {
            nameNodes.push((<LeftName name={item} click={(event) => this._onRightNameClick(event)}/>));
        }
        return nameNodes;
    }

    _onRightNameClick(event) {
        console.log(event.target.innerText);
        event.target.style.background = '#16a4fa';
        event.target.style.color = '#fff';
        this.setState({willbeRemovedName : event.target.innerText});
    }

    _onBackgroundClick() {
        // window.parent.postMessage({msg: "dismiss", sender: "w2"}, window.location.origin);
        // console.log("=====classMate 发送消息====");
    }

    _onBtnAddClick() {
        this.state.selectedMembers.add(this.state.willbeAddName);
        this.setState({selectedMembers : this.state.selectedMembers});
    }

    _onBtnRemoveClick() {
        // 移除选中的目标

        this.state.selectedMembers.delete(this.state.willbeRemovedName);
        this.setState({selectedMembers : this.state.selectedMembers});
    }

    /*_onFooterBtnClick() {
        //发送消息
        debugger;
        window.parent.postMessage({msg: this.state.selectedMembers, sender: "w2"}, window.location.origin);
        this.state.isChildDialogVisible = false;
    }*/

    render() {
        let style_img_delete = {
            width: 20,
            height: 20,
            position: 'absolute',
            top: 3,
            right: 3
        };
        let style_line = {
            width: 100,
            height: 1,
            backgroundColor : '#e2e2e2'
        };
        let style_center_box = {
            height: '100%',
            display: 'flex',
            flexDirection : 'column',
            justifyContent : 'center'
        };
        let style_bg = {
            width : '100%',
            height : '100%'
        };

        return (
            <div className={style_bg}>
                <div id="classmate_background" onClick={() => this._onBackgroundClick()}>
                </div>
                <div className="classmate_dialog">
                    <div className="classmate_dialog_head">
                        <label>班级成员</label>
                        <img src={require("./images/close.png")} style={style_img_delete}/>
                    </div>
                    <div className="classmate_dialog_center">
                        <div className="user_list_box">
                            <div className="search_box">
                                <input type="text"/>
                                <img src={require("./images/search.png")}/>
                            </div>
                            <div style={style_line}></div>
                            <div className="user_list">
                                {this._createLeftName()}
                            </div>
                        </div>
                        <div style={style_center_box}>
                            <button type="button" id="btn_add" onClick={() => this._onBtnAddClick()}>添加</button>
                            <button type="button" id="btn_remove" onClick={() => this._onBtnRemoveClick()}>删除</button>
                        </div>
                        <div className="user_list_box_right">
                            {this._createRightName()}
                        </div>
                    </div>
                    <div className="classmate_dialog_footer" >
                        <div className="classmate_dialog_footer_btn" onClick={() => this.props.click(this.state.selectedMembers)}>确定</div>
                    </div>
                </div>

            </div>
        )
    }
}
export default Classmates;