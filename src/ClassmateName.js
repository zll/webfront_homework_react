/**
 * Created by Administrator on 2017/8/24.
 */
import React, {Component} from "react";
import "./theme/style/classmates.css";

class LeftName extends Component {

    constructor(props) {
        super(props);
        this.state = {
            style : {
                width: '100%',
                height: 25,
                color: '#999',
                fontSize : 13,
                margin: 3
            }
        }
    }

    _onNameClick(style) {
        //style.background = "#16a4fa";
        this.setState({background : '#16a4fa'});
    }

    render () {

        let style_name = this.state.style;
        return (
            <label className="left_name" style={style_name}
                   onClick={(event) => this.props.click(event)}> {this.props.name} </label>
        )
    }
}
export default LeftName;