import React from "react";
import ReactDOM from "react-dom";
import "./theme/style/index.css";
import Main from "./modules/container/Main/Main";
import {Provider} from "react-redux";
import {createStore} from "redux";
import reducer from "./reducers/reducer";
import registerServiceWorker from "./registerServiceWorker";

const store = createStore(reducer);
console.log(reducer);
const render = () => ReactDOM.render(
    <Provider store={store} >
    <Main />
    </Provider>,
    document.getElementById('root'));
render();

store.subscribe(render);
registerServiceWorker();

