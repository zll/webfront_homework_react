import React, {Component} from "react";
import "./theme/style/App.css";

class Other extends Component {

    render() {
        var line = {
            height: 1,
            background: '#e2e2e2'
        };
        var image = {
            width: 20,
            height: 20,
        };
        let border = {
            borderWidth: 1 ,
            borderStyle : 'solid',
            borderColor: '#e2e2e2'
        };
        let item_name = {
            margin: 0,
            paddingTop: 10,
            paddingBottom: 10,
            textAlign : 'center',
            color: '#888',
            fontSize: 12,
        };
        return (
            <div className="page_right_item">
                <div style={border}>
                    <div className="page_right_item_top">
                        <img className="page_right_item_top_img" src={require("./images/bg_mountain.png")}/>
                        <div className="page_right_item_top_text">{this.props.title}</div>
                    </div>
                    <div style={line}></div>
                    <div className="page_right_item_bottom" >
                        <div className="page_right_item_bottom_btn">
                            <img src={require("./images/edit.png")} style={image}/>
                            <div>编辑</div>
                        </div>
                        <div className="page_right_item_bottom_btn">
                            <img src={require("./images/play_pressed.png")} style={image}/>
                            <div>播放</div>
                        </div>
                        <div className="page_right_item_bottom_btn">
                            <img src={require("./images/delete.png")} style={image}/>
                            <div>删除</div>
                        </div>
                    </div>

                </div>
                <p style={item_name}>{this.props.name}</p>
            </div>
        )
    }
}
export default Other;
