import React from "react";
import {connect} from "react-redux";
import MainContent from "../.././components/MainContent/MainContent";
// import {courseMenuGet} from '../.././actions/index'
import {initCourses} from "../../.././reducers/reducer";

const Main = (props) => (
  <MainContent {...props} />
)

const mapStateToProps = state => ({
  courseMenu: state.items
})

const mapDispatchToProps = (dispatch) => {
  return {
    courseMenuGet : (courses) => {
      dispatch(initCourses(courses))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main)
