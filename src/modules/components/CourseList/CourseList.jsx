import React, {Component, PropTypes} from "react";
import List from "../.././common/List/List";

export default class CourseList extends Component {
  static propTypes = {
    courseMenu: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired
    })).isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      selected: ''
    }
  }

  handleClick = (id) => {
    this.setState({
      selected: id
    })
  }

  render() {
    const courseMenu = [...this.props.courseMenu]
    courseMenu.unshift({id: -1, name: '全部课程'})
    return (
      <ul style={{paddingLeft : 0 + 'px', marginTop: 0 + 'px'}}>
        {courseMenu.map(
          (course) =>
            <List
              key={course.id}
              course={course}
              selected={this.state.selected}
              onClick={() => { this.handleClick(course.id) }} />
        )}
      </ul>
    )
  }
}
