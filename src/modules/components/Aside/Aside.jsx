import React, {Component} from "react";
import "./Aside.css";
import CourseList from "../CourseList/CourseList";

// @CSSModules(styles, {allowMultiple: true})

export default class Aside extends Component {
  static propTypes = {
    courseMenu: React.PropTypes.array,
    style: React.PropTypes.object
  }
  render() {
    return (
      <div className="aside" style={this.props.style}>
        <div className="logo">
          <img src={require("../../.././images/101_logo.png")} alt="101_logo" />
          <h1>VR创客教室</h1>
        </div>
        <div className="course">课程列表</div>
        <CourseList courseMenu={this.props.courseMenu} />
      </div>
    )
  }
}
