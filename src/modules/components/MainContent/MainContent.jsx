import React, {Component} from "react";
import "./MainContent.css";
import Aside from "../Aside/Aside";
import App from "../../.././App";

// @CSSModules(styles, {allowMultiple: true})

export default class MainContent extends Component {
  static propTypes = {
    courseMenu: React.PropTypes.array,
    courseMenuGet: React.PropTypes.func
  }

  componentDidMount() {
    let courseMenu = [
        {"id": 1, "name": "课程一：静夜思"},
        {"id": 2, "name": "课程二：红军长征"},
        {"id": 3, "name": "课程三：活字印刷术"},
        {"id": 4, "name": "课程四：郑和下西洋"},
        {"id": 5, "name": "课程五：梦想之家"}
    ]
    this.props.courseMenuGet(courseMenu)//TODO 添加参数
  }

  render() {
    return (
      <div className="main">
        <Aside courseMenu={this.props.courseMenu} style={{}} />
        <App />
      </div>
    )
  }
}
