import React, {Component} from "react";
import "./TabSwitch.css";
//import CSSModules from 'react-css-modules'

//@CSSModules(styles, {allowMultiple: true})

export default class TabSwitch extends Component {
  static propTypes = {
    tabs: React.PropTypes.arrayOf(
      React.PropTypes.object
    ),
    onClick: React.PropTypes.func
  }

  static defaultProps = {
    onClick: () => {}
  }

  constructor(props) {
    super(props)
    this.state = {
      selected: 1
    }
  }

  handleClick = (id) => {
    this.setState({
      selected: id
    })
    this.props.onClick(id)
  }

  render() {
    return (
      <ul className="tab-switch">
        {this.props.tabs.map((tab, index) => (
          <li
            key={tab.id}
            className={(index % 2 === 0 ? 'tab-odd' : '') +
              (this.state.selected === tab.id ? ' tab-select' : '')}
            onClick={() => { this.handleClick(tab.id) }}><span>{tab.text}</span></li>
        ))}
      </ul>
    )
  }
}
