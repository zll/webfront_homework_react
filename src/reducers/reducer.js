import * as types from "../modules/constants/actionTypes";
/**
 * Created by Administrator on 2017/8/29.
 */
export default function (state = {items : []}, action) {
    switch (action.type) {
        case types.COURSEMENU_GET:
            console.log("获取课程");
            return {
                ...state,
                items: action.courses
            }

        default:
            return state;
    }
}

// action creators
export const initCourses = (courses) => {
    return { type: types.COURSEMENU_GET, courses }
}
